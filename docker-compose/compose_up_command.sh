#!/bin/bash
GIT_COMMIT_DATE="$(git log -1 --date=format:"%Y%m%d" --format=%ad)" GIT_COMMIT_HASH="$(git log -1 --format=%H|cut -c 1-8)" docker-compose up -d