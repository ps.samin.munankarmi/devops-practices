FROM anapsix/alpine-java:8_server-jre
ARG GIT_COMMIT_DATE
ARG GIT_COMMIT_HASH
ENV GIT_COMMIT_DATE $GIT_COMMIT_DATE
ENV GIT_COMMIT_HASH $GIT_COMMIT_HASH
RUN apk update && apk add busybox-extras iputils net-tools bind-tools
RUN addgroup devops && adduser -D devops -G devops
USER devops
WORKDIR /opt/app
COPY target/$GIT_COMMIT_DATE-$GIT_COMMIT_HASH.jar .
CMD java -jar -Dspring.profiles.active=$H2_PROFILE -Dserver.port=$PORT /opt/app/$GIT_COMMIT_DATE-$GIT_COMMIT_HASH.jar
