#!/bin/bash
docker node update --availability drain t08zlw9z4jhfq0kat7mxm4cb0
env $(cat .env|xargs) GIT_COMMIT_DATE="$(git log -1 --date=format:"%Y%m%d" --format=%ad)" GIT_COMMIT_HASH="$(git log -1 --format=%H|cut -c 1-8)" docker stack deploy --with-registry-auth --compose-file docker-compose.yml appstack
