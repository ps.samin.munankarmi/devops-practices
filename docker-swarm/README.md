### **Docker Swarm**

1. To initiate swarm in the node which is supposed to be manager:

   ```bash
   docker swarm init --advertise-addr 192.168.57.1
   ```

   > Swarm initialized: current node (t08zlw9z4jhfq0kat7mxm4cb0) is now a manager.
   >
   > To add a worker to this swarm, run the following command:
   >
   >     docker swarm join --token SWMTKN-1-4lrnfcmjpwq1tz1fydabvetn0c4064ylu3u45sxk8oeikkes9r-2qac02q1ltt4poadn59urfkom 192.168.57.1:2377
   >
   > To add a manager to this swarm, run 'docker swarm join-token manager' and follow the instructions.

2. To add nodes to this initialized swarm we need to run the below command in every server supposed to be a worker as given in the above output:

   ```bash
   docker swarm join --token SWMTKN-1-4lrnfcmjpwq1tz1fydabvetn0c4064ylu3u45sxk8oeikkes9r-2qac02q1ltt4poadn59urfkom 192.168.57.1:2377
   ```

   